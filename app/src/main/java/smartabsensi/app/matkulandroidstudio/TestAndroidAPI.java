package smartabsensi.app.matkulandroidstudio;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import static com.android.volley.Request.Method.GET;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import smartabsensi.app.matkulandroidstudio.model.Person;

/**
 * Created by Endra on 03/11/20.
 */

public class TestAndroidAPI extends AppCompatActivity {

    private RequestQueue requestQueue;
    ArrayList<Person> listPeople = new ArrayList<>();
    RecyclerView listview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_api);

        //Request Data
        initRequest();

        //GetDataPeople
        getDataPeople();
    }

    private void getDataPeople() {
        String url = "https://my-json-server.typicode.com/EndraDeveloper/MobileDev/db";

        JSONObject jsonObject = new JSONObject();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("people");
                    for (int i=0; i<data.length(); i++){
                        JSONObject type = data.getJSONObject(i);
                        //simpan data to Model
                        Person person = new Person(
                                type.getInt("id"),
                                type.getString("name"),
                                type.getString("email"),
                                type.getString("gender"));
                        listPeople.add(person);
                    }
                    PeopleAdater adapter = new PeopleAdater(TestAndroidAPI.this, listPeople);

                    listview.setAdapter(adapter);
                    listview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TestAndroidAPI.this,"Terjadi kesalahan",Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void initRequest() {
        listview = findViewById(R.id.listPeople);
        requestQueue = Volley.newRequestQueue(this);
    }
}
