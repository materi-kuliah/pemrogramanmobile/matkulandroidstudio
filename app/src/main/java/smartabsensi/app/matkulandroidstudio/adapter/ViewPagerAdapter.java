package smartabsensi.app.matkulandroidstudio.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import smartabsensi.app.matkulandroidstudio.fragment.CameraFragment;
import smartabsensi.app.matkulandroidstudio.fragment.HomeFragment;
import smartabsensi.app.matkulandroidstudio.fragment.ProfileFragment;

/**
 * Created by Endra on 2020-10-06.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new HomeFragment();
            case 1 :
                return new CameraFragment();
            case 2 :
                return new ProfileFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
