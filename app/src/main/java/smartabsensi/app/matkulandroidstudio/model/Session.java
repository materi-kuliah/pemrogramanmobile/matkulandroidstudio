package smartabsensi.app.matkulandroidstudio.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Endra on 05/12/20.
 */

public class Session {

    private static final String EXTRA_MATKUL = "extra_matkul";
    private static final String EXTRA_ID = "user_id";
    private static final String EXTRA_NAME = "user_name";
    private static final String EXTRA_EMAIL = "user_email";
    private static final String EXTRA_NOTELP = "user_phone";
    private static final String IS_LOGIN = "user_islogin";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public Session(Context context){
        sharedPreferences = context.getSharedPreferences(EXTRA_MATKUL, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setUserId(String userId){
        editor.putString(EXTRA_ID,userId);
        editor.commit();
    }

    public void setUserName(String userName){
        editor.putString(EXTRA_NAME, userName);
        editor.commit();
    }

    public void setUserEmail(String userEmail){
        editor.putString(EXTRA_EMAIL, userEmail);
        editor.commit();
    }

    public void setUserPhone(String userPhone){
        editor.putString(EXTRA_NOTELP, userPhone);
        editor.commit();
    }

    public void setIsLogin(Boolean isLogin){
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }

    public String getExtraId(){
        return sharedPreferences.getString(EXTRA_ID,"");
    }

    public String getExtraName(){
        return sharedPreferences.getString(EXTRA_NAME,"");
    }

    public String getExtraEmail(){
        return sharedPreferences.getString(EXTRA_EMAIL,"");
    }

    public String getExtraNotelp(){
        return sharedPreferences.getString(EXTRA_NOTELP,"");
    }

    public Boolean getLogIn(){
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }
}
