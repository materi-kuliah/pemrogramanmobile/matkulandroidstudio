package smartabsensi.app.matkulandroidstudio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import smartabsensi.app.matkulandroidstudio.model.Person;

/**
 * Created by Endra on 17/11/20.
 */

public class PeopleAdater extends RecyclerView.Adapter<PeopleAdater.PeopleViewHolder> {

    Context context;
    ArrayList<Person> listPeople;

    public PeopleAdater(Context context, ArrayList<Person> listPeople) {
        this.context = context;
        this.listPeople=listPeople;
    }


    @NonNull
    @Override
    public PeopleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem, parent, false);

        return new PeopleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleViewHolder holder, int position) {
        TextView txName = holder.itemView.findViewById(R.id.txName);
        TextView txEmail = holder.itemView.findViewById(R.id.txEmail);
        TextView txGender = holder.itemView.findViewById(R.id.txGender);

        Person person = listPeople.get(position);
        txName.setText(String.valueOf(position)+". "+person.getName());
        txEmail.setText(person.getEmail());
        txGender.setText(person.getGender());

    }

    @Override
    public int getItemCount() {
        return listPeople.size();
    }

    class PeopleViewHolder extends RecyclerView.ViewHolder{
        public PeopleViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
