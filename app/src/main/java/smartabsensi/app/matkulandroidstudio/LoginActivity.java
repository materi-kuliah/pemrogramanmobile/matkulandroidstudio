package smartabsensi.app.matkulandroidstudio;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import smartabsensi.app.matkulandroidstudio.model.Person;
import smartabsensi.app.matkulandroidstudio.model.Session;

import static com.android.volley.Request.Method.POST;

/**
 * Created by Endra on 2020-10-09.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public CircularProgressButton btn;
    TextInputLayout tilEmail, tilPassword;
    TextInputEditText txtEmail, txtPassword;

    String url = "https://selalusegar.com/testing/public/api/auth/login";

    private RequestQueue requestQueue;
    private Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Request Data
        initRequest();

        //set button for click
        btn.setOnClickListener(this);
    }

    private void initRequest() {
        //inisialisasi object/atribute
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btn = findViewById(R.id.btn_id);

        //inisialisasi Que Volley
        requestQueue = Volley.newRequestQueue(this);
        session = new Session(this);
    }

    @Override
    public void onClick(View view) {
        if(view == btn){
            cekLogin();
            btn.startAnimation();
        }
    }

    private void cekLogin() {
        String email = txtEmail.getText().toString();
        String pwd = txtPassword.getText().toString();

        if (email.isEmpty()){
            tilEmail.setError("Email kosong");
        }else{
            tilEmail.setErrorEnabled(false);
        }

        if (pwd.isEmpty()){
            tilPassword.setError("Password kosong");
        }else{
            tilPassword.setErrorEnabled(false);
        }

        if (!email.equals("") && !pwd.equals("")){
            sendUserNameAndPassword(email, pwd);
        }
    }

    private void sendUserNameAndPassword(String email, String pwd) {
        HashMap<String, String> param = new HashMap<>();
        param.put("email",email);
        param.put("password", pwd);

        JSONObject jsonObject = new JSONObject(param);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equals("1")){
                        JSONObject data = response.getJSONObject("data");
                        String user_id = data.getString("id");
                        String user_name = data.getString("name");
                        String user_email = data.getString("email");
                        String user_phone = data.getString("phone");

                        //saveDataToSession
                        saveToSesion(user_id, user_name, user_email, user_phone);

                        openNewActivity();
                        Log.e("success", user_id+" "+user_name+" "+user_email+" "+user_phone);
                    }else {
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                    btn.revertAnimation();
                } catch (JSONException e) {
                    e.printStackTrace();
                    btn.revertAnimation();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this,"Terjadi kesalahan",Toast.LENGTH_LONG).show();
                btn.revertAnimation();
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void saveToSesion(String user_id, String user_name, String user_email, String user_phone) {
        session.setUserId(user_id);
        session.setUserName(user_name);
        session.setUserEmail(user_email);
        session.setUserPhone(user_phone);
        session.setIsLogin(true);
    }

    private void openNewActivity() {
        Intent home = new Intent(this, TestAndroidAPI.class);
        startActivity(home);
    }
}
