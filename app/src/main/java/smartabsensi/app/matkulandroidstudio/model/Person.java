package smartabsensi.app.matkulandroidstudio.model;

/**
 * Created by Endra on 17/11/20.
 */

public class Person {

    private Integer id;
    private String name;
    private String email;
    private String gender;

    public Person(Integer id,String name, String email, String gender){
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }
    public String getGender() {
        return gender;
    }

}
